import mangaGetter as backend

chapters = backend.getManga('https://readmanganato.com/manga-jh951716/')

chapterImages = backend.getChapter(chapters['1'])

images = backend.getImages(chapterImages)

files = backend.loadImages(images)

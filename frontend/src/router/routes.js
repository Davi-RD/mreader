
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Mangas.vue') }
    ]
  },

  {
    path: '/chapters',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/chapters/:url', component: () => import('pages/Chapters.vue') }
    ]
  },

  {
    path: '/chapterlist',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/chapterlist/:url', component: () => import('pages/ChapterList.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes

from io import BytesIO
from PIL import Image
import requests

def getManga(url):
    page = requests.get(url).text
    listChapters = list()
    chapters = dict()

    for word in page.split():
        if('/chapter-' in word):
            listChapters.append(word[6:-1:])

    for chap in listChapters:
        index = chap.find('chapter-')
        chapters[chap[index + 8::]] = chap

    return chapters

def getChapter(url):
    chapter = requests.get(url).text
    images = list()
    for word in chapter.split():
        if 'chapter' in word and 'jpg' in word:
            images.append(word[5:-1:])
    return images

def getImages(imageList):
    images = list()
    for image in imageList:
        images.append(requests.get(image, headers={'referer':'https://readmanganato.com'}).content)
    return images

def loadImages(images):
    files = list()
    for img in images:
        files.append(Image.open(BytesIO(img)))
    return files

from django.db import models

class Manga(models.Model):
    URL = models.CharField('Endereço', max_length=500, primary_key = True)
    name = models.CharField('Nome', max_length=300)
    lastReadChapter = models.IntegerField('Último capítulo lido', default=0)
    lastChapter = models.IntegerField('Último capítulo lançado', default=0)

class Chapter(models.Model):
    URL = models.CharField('Endereço', primary_key = True, max_length=500)
    name = models.CharField('Nome', blank=True, max_length=300)
    number = models.FloatField()
    images = models.TextField('Images', blank=True)
    manga = models.ForeignKey(Manga, on_delete=models.CASCADE)
    

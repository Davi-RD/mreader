from .models import Manga, Chapter
from io import BytesIO
from ninja import NinjaAPI, Query, Schema
import requests
import base64

api = NinjaAPI()

class Data(Schema):
    url: str
    title: str = None
    number: str = None


@api.post('/saveManga')
def saveManga(request, data: Data):
    url = data.dict()['url']
    title = data.dict()['title']
    newManga = Manga(name = title, URL = url)
    newManga.save()

@api.post('/getMangas')
def saveManga(request):
    mangas = list()
    for manga in Manga.objects.all():
        mangas.append({
        'nome': manga.name,
        'URL': manga.URL
        })
    return mangas

@api.post('/getChapters')
def getChapters(request, data: Data):
    url = data.dict()['url']
    page = requests.get(url).text
    listChapters = list()
    chapters = list()
    rightManga = url[url.find('manga-')::]

    for word in page.split():
        mangaStart = word.find('manga-')
        mangaEnd = word.find('/chapter')
        manga = word[mangaStart:mangaEnd:]
        if('/chapter-' in word and manga == rightManga and "/chapter-';" not in word):
            listChapters.append(word[6:-1:])

    for chapter in listChapters:
        chap = dict()
        index = chapter.find('chapter-')
        chap['numero'] = chapter[index + 8::]
        chap['URL'] = chapter
        chap['downloadStatus'] = {True:'done', False:'download'}[bool(Chapter.objects.filter(URL=chapter))]
        chapters.append(chap)
    return chapters

@api.post('/loadImages')
def loadImages(request, data: Data, url=''):
    url = data.dict()['url']

    if(Chapter.objects.filter(URL=url)):
        chapter = Chapter.objects.get(URL=url)
        images = chapter.images.split(' ')
        return images

    else:
        imageList = getImages(url)
        images = list()
        for image in imageList:
            images.append(requests.get(image, headers={'referer':'https://readmanganato.com'}).content)
        files = list()
        for img in images:
            files.append("data:image/jpg;base64," + base64.b64encode(img).decode('utf-8'))

        return files



@api.post('/deleteManga')
def deleteManga(request, data: Data):
    url = data.dict()['url']
    manga = Manga.objects.get(URL = url)
    manga.delete()

@api.post('/deleteChapter')
def deleteManga(request, data: Data):
    url = data.dict()['url']
    chapter = Chapter.objects.get(URL = url)
    chapter.delete()

@api.post('/saveChapter')
def saveChapter(request, data: Data):
    urlChapter = data.dict()['url']
    urlManga = urlChapter.split('/')
    urlManga.pop()
    urlManga = '/'.join(urlManga)
    index = urlChapter.find('chapter-')
    number = urlChapter[index + 8::]
    images = loadImages(request, data)
    chapter = ' '.join(images)
    manga = Manga.objects.get(URL=urlManga)
    manga.chapter_set.create(URL=urlChapter, images=chapter, number=number)


def getImages(url):
    chapter = requests.get(url).text
    images = list()
    for word in chapter.split():
        if 'chapter' in word and 'jpg' in word:
            images.append(word[5:-1:])
    return images
